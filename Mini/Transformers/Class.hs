-- | The class of monad transformers
module Mini.Transformers.Class (
  -- * Class
  MonadTrans (
    lift
  ),
) where

import Prelude (
  Monad,
 )

{-
 - Class
 -}

{- | Instances should satisfy the following laws:

> lift . pure = pure

> lift (m >>= f) = lift m >>= (lift . f)
-}
class MonadTrans t where
  -- | Lift a computation from the inner monad to the transformer monad
  lift :: (Monad m) => m a -> t m a
