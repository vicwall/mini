-- | Curated re-export of immutable non-strict (boxed) arrays from "GHC.Arr"
module Mini.Data.Array (
  -- * Type
  Array,

  -- * Class
  Ix (
    inRange,
    index,
    range,
    rangeSize,
    unsafeIndex,
    unsafeRangeSize
  ),

  -- * Construction
  array,
  listArray,
  accumArray,

  -- * Conversion
  assocs,
  elems,
  indices,

  -- * Modification
  (//),
  accum,
  ixmap,

  -- * Query
  (!),
  (!?),
  bounds,
  numElements,
) where

import Data.Bool (
  bool,
 )
import GHC.Arr (
  Array,
  Ix (
    inRange,
    index,
    range,
    rangeSize,
    unsafeIndex,
    unsafeRangeSize
  ),
  accum,
  accumArray,
  array,
  assocs,
  bounds,
  elems,
  indices,
  ixmap,
  listArray,
  numElements,
  unsafeAt,
  (!),
  (//),
 )
import Prelude (
  Maybe (
    Just,
    Nothing
  ),
  ($),
  (.),
 )

infixl 9 !?

-- | The value at the given index in an array, unless out of range.
(!?) :: (Ix i) => Array i e -> i -> Maybe e
arr !? i =
  let b = bounds arr
   in bool
        Nothing
        (Just . unsafeAt arr $ unsafeIndex b i)
        $ inRange b i
