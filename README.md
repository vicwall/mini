# mini

Everyday essentials: data structures, primitive recursion, lenses, transformers,
and parsing.

Uncompromisingly light on dependencies.

Easily navigable code base, keeping indirection and clutter to a minimum.

## Contents

* Data
    * [Array](Mini/Data/Array.hs)
    * [Graph](Mini/Data/Graph.hs)
    * [Map](Mini/Data/Map.hs)
    * [Recursion](Mini/Data/Recursion.hs)
    * [Set](Mini/Data/Set.hs)
* Optics
    * [Lens](Mini/Optics/Lens.hs)
* Transformers
    * [Class](Mini/Transformers/Class.hs)
    * [EitherT](Mini/Transformers/EitherT.hs)
    * [MaybeT](Mini/Transformers/MaybeT.hs)
    * [ParserT](Mini/Transformers/ParserT.hs)
    * [ReaderT](Mini/Transformers/ReaderT.hs)
    * [StateT](Mini/Transformers/StateT.hs)
    * [WriterT](Mini/Transformers/WriterT.hs)

## Documentation

Documentation is available on [Hackage.](https://hackage.haskell.org/package/mini)
